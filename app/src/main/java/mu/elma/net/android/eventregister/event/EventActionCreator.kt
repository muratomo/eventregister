package mu.elma.net.android.eventregister.event

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.FragmentActivity
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch
import mu.elma.net.android.eventregister.Action
import mu.elma.net.android.eventregister.Repository

sealed class EventAction<T> : Action<T> {
    class SetEventsAction(override val data: List<Event>) : EventAction<List<Event>>()
}

class EventActionCreator(appContext: Context, activity: FragmentActivity) {
    private val store = ViewModelProviders.of(activity).get(EventStore::class.java)
    private val repository = Repository(appContext)

    fun getAllEvents() = GlobalScope.launch {
        val allEvents = repository.getAllEvents()
        store.dispatch(EventAction.SetEventsAction(allEvents))
    }
}
