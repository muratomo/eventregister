package mu.elma.net.android.eventregister

import android.content.Context
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.async
import mu.elma.net.android.eventregister.event.Event

class Repository(context: Context) {
        private val database = DataBase.getDataBase(context)

    suspend fun getAllEvents(): List<Event> = GlobalScope.async {
        return@async database.eventDao().findAll()
    }.await()
}