package mu.elma.net.android.eventregister.event

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Event(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        val name: String
)
