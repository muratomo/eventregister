package mu.elma.net.android.eventregister

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import mu.elma.net.android.eventregister.event.Event
import mu.elma.net.android.eventregister.event.EventDAO

@Database(entities = [Event::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract fun eventDao(): EventDAO

    // 都度生成しないようにstaticでDBインスタンスを保持
    companion object {
        private val dataBase: DataBase? = null

        @Synchronized
        fun getDataBase(appContext: Context) =
                dataBase ?: Room.databaseBuilder(
                        appContext.applicationContext,
                        DataBase::class.java,
                        "event_register.db")
                        .build()
    }
}