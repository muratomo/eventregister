package mu.elma.net.android.eventregister.event

import android.arch.persistence.room.*

@Dao
interface EventDAO {
    @Insert(onConflict = OnConflictStrategy.ROLLBACK)
    fun create(event: Event)

    @Update
    fun update(event: Event)

    @Delete
    fun delete(event: Event)

    @Query("SELECT * FROM Event")
    fun findAll(): List<Event>
}