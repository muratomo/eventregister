package mu.elma.net.android.eventregister

interface Action<T> {
    val data: T
}