package mu.elma.net.android.eventregister.event

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

import mu.elma.net.android.eventregister.R

class EventActivity : AppCompatActivity() {
    private val store by lazy {
        ViewModelProviders.of(this).get(EventStore::class.java) }
    private val eventActionCreator by lazy { EventActionCreator(applicationContext, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        val adapter = EventRecycleViewAdapter()
        val linearLayoutManager = LinearLayoutManager(this)
        val eventRecycleView = findViewById<RecyclerView>(R.id.eventRecyclerView).apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
        }
        eventRecycleView.adapter = adapter

        store.events.observe(this, Observer {
            it ?: return@Observer
            adapter.run {
                clear()
                setData(it)
                notifyDataSetChanged()
            }
        })
        eventActionCreator.getAllEvents()
    }
}
