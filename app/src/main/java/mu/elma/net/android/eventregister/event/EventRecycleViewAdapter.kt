package mu.elma.net.android.eventregister.event

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import mu.elma.net.android.eventregister.R

class EventRecycleViewAdapter : RecyclerView.Adapter<EventRecycleViewAdapter.EventViewHolder>() {
    class EventViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val nameView: TextView? = itemView?.findViewById(R.id.eventName)
    }

    private val events: ArrayList<Event> = arrayListOf()

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.nameView?.text = events[position].name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.event, parent, false)
        return EventViewHolder(view)
    }

    override fun getItemCount() = events.size

    fun setData(events: List<Event>) {
        events.forEach { it -> this.events.add(it) }
    }

    fun clear() {
        events.clear()
    }
}
