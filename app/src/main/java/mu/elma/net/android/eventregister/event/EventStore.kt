package mu.elma.net.android.eventregister.event

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class EventStore : ViewModel() {
    // 内部で操作するデータと外へ送るデータを分ける
    private val internalEvents = MutableLiveData<List<Event>>()
    val events: LiveData<List<Event>> = internalEvents

    fun <T> dispatch(action: EventAction<T>) {
        when(action) {
            is EventAction.SetEventsAction -> {
                internalEvents.postValue(action.data)
            }
        }
    }
}