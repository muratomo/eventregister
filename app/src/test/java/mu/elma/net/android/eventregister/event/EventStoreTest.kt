package mu.elma.net.android.eventregister.event

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class EventStoreTest {
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val eventObserver = mockk<Observer<List<Event>>>(relaxed = true)


    private val store = EventStore()

    @Before
    fun setUp() {
        store.events.observeForever(eventObserver)
    }

    @After
    fun tearDown() {
        store.events.removeObserver(eventObserver)
    }

    @Test
    fun Eventリストをセットできる() {
        val testEvents = arrayListOf(
                Event(id = 100, name = "test 100"),
                Event(id = 200, name = "test 200")
        )
        val action: EventAction<List<Event>> = EventAction.SetEventsAction(testEvents)
        store.dispatch(action)
        verify { eventObserver.onChanged(testEvents) }
        assertEquals("Eventリストが反映されていない", testEvents, store.events.value)
    }
}
